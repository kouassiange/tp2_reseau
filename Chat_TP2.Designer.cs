﻿namespace TP2_reseau
{
    partial class Chat_TP2
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Chat_TP2));
            this.panel_Nom = new System.Windows.Forms.Panel();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.label_Nom = new System.Windows.Forms.Label();
            this.btnConnexion = new System.Windows.Forms.Button();
            this.btnDeconnexion = new System.Windows.Forms.Button();
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.panel_pers_connecte = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxMessages = new System.Windows.Forms.ListBox();
            this.textBoxEnvoi = new System.Windows.Forms.TextBox();
            this.timerUser = new System.Windows.Forms.Timer(this.components);
            this.btnEnvoi = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnJoindre = new System.Windows.Forms.Button();
            this.label_Ami_connecte = new System.Windows.Forms.Label();
            this.panel_Nom.SuspendLayout();
            this.panel_pers_connecte.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Nom
            // 
            this.panel_Nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_Nom.Controls.Add(this.textBoxNom);
            this.panel_Nom.Controls.Add(this.label_Nom);
            this.panel_Nom.Location = new System.Drawing.Point(21, 12);
            this.panel_Nom.Name = "panel_Nom";
            this.panel_Nom.Size = new System.Drawing.Size(257, 55);
            this.panel_Nom.TabIndex = 0;
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(74, 17);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(163, 20);
            this.textBoxNom.TabIndex = 8;
            // 
            // label_Nom
            // 
            this.label_Nom.AutoSize = true;
            this.label_Nom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Nom.Location = new System.Drawing.Point(13, 17);
            this.label_Nom.Name = "label_Nom";
            this.label_Nom.Size = new System.Drawing.Size(44, 16);
            this.label_Nom.TabIndex = 0;
            this.label_Nom.Text = "Nom ";
            // 
            // btnConnexion
            // 
            this.btnConnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnexion.Location = new System.Drawing.Point(342, 12);
            this.btnConnexion.Name = "btnConnexion";
            this.btnConnexion.Size = new System.Drawing.Size(125, 39);
            this.btnConnexion.TabIndex = 3;
            this.btnConnexion.Text = "Connexion";
            this.btnConnexion.UseVisualStyleBackColor = true;
            this.btnConnexion.Click += new System.EventHandler(this.btnConnexion_Click);
            // 
            // btnDeconnexion
            // 
            this.btnDeconnexion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeconnexion.Location = new System.Drawing.Point(490, 12);
            this.btnDeconnexion.Name = "btnDeconnexion";
            this.btnDeconnexion.Size = new System.Drawing.Size(125, 39);
            this.btnDeconnexion.TabIndex = 4;
            this.btnDeconnexion.Text = "Déconnexion";
            this.btnDeconnexion.UseVisualStyleBackColor = true;
            this.btnDeconnexion.Click += new System.EventHandler(this.btnDeconnexion_Click);
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.Location = new System.Drawing.Point(473, 121);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(187, 199);
            this.listBoxUsers.TabIndex = 5;
            this.listBoxUsers.DoubleClick += new System.EventHandler(this.listBoxUsers_DoubleClick);
            // 
            // panel_pers_connecte
            // 
            this.panel_pers_connecte.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel_pers_connecte.Controls.Add(this.label1);
            this.panel_pers_connecte.Location = new System.Drawing.Point(473, 73);
            this.panel_pers_connecte.Name = "panel_pers_connecte";
            this.panel_pers_connecte.Size = new System.Drawing.Size(187, 42);
            this.panel_pers_connecte.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Personne(s) connectée(s)";
            // 
            // listBoxMessages
            // 
            this.listBoxMessages.FormattingEnabled = true;
            this.listBoxMessages.Location = new System.Drawing.Point(21, 73);
            this.listBoxMessages.Name = "listBoxMessages";
            this.listBoxMessages.Size = new System.Drawing.Size(446, 277);
            this.listBoxMessages.TabIndex = 6;
            // 
            // textBoxEnvoi
            // 
            this.textBoxEnvoi.Location = new System.Drawing.Point(21, 354);
            this.textBoxEnvoi.Name = "textBoxEnvoi";
            this.textBoxEnvoi.Size = new System.Drawing.Size(359, 20);
            this.textBoxEnvoi.TabIndex = 8;
            this.textBoxEnvoi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEnvoi_KeyDown);
            // 
            // timerUser
            // 
            this.timerUser.Interval = 5000;
            this.timerUser.Tick += new System.EventHandler(this.timerUser_Tick);
            // 
            // btnEnvoi
            // 
            this.btnEnvoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnvoi.Location = new System.Drawing.Point(386, 353);
            this.btnEnvoi.Name = "btnEnvoi";
            this.btnEnvoi.Size = new System.Drawing.Size(81, 20);
            this.btnEnvoi.TabIndex = 9;
            this.btnEnvoi.Text = "Envoi";
            this.btnEnvoi.UseVisualStyleBackColor = true;
            this.btnEnvoi.Click += new System.EventHandler(this.btnEnvoi_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnJoindre);
            this.panel1.Controls.Add(this.label_Ami_connecte);
            this.panel1.Location = new System.Drawing.Point(473, 325);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(187, 48);
            this.panel1.TabIndex = 9;
            // 
            // btnJoindre
            // 
            this.btnJoindre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnJoindre.Location = new System.Drawing.Point(49, 19);
            this.btnJoindre.Name = "btnJoindre";
            this.btnJoindre.Size = new System.Drawing.Size(81, 20);
            this.btnJoindre.TabIndex = 13;
            this.btnJoindre.Text = "Joindre";
            this.btnJoindre.UseVisualStyleBackColor = true;
            this.btnJoindre.Click += new System.EventHandler(this.btnJoindre_Click);
            // 
            // label_Ami_connecte
            // 
            this.label_Ami_connecte.AutoSize = true;
            this.label_Ami_connecte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Ami_connecte.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label_Ami_connecte.Location = new System.Drawing.Point(23, 0);
            this.label_Ami_connecte.Name = "label_Ami_connecte";
            this.label_Ami_connecte.Size = new System.Drawing.Size(143, 16);
            this.label_Ami_connecte.TabIndex = 9;
            this.label_Ami_connecte.Text = "Ami(e) connecté(e) ";
            // 
            // Chat_TP2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 381);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnEnvoi);
            this.Controls.Add(this.textBoxEnvoi);
            this.Controls.Add(this.listBoxMessages);
            this.Controls.Add(this.panel_pers_connecte);
            this.Controls.Add(this.listBoxUsers);
            this.Controls.Add(this.btnDeconnexion);
            this.Controls.Add(this.btnConnexion);
            this.Controls.Add(this.panel_Nom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Chat_TP2";
            this.Text = "Chat2_TP2";
            this.panel_Nom.ResumeLayout(false);
            this.panel_Nom.PerformLayout();
            this.panel_pers_connecte.ResumeLayout(false);
            this.panel_pers_connecte.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_Nom;
        private System.Windows.Forms.Button btnConnexion;
        private System.Windows.Forms.Button btnDeconnexion;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.Label label_Nom;
        private System.Windows.Forms.ListBox listBoxUsers;
        private System.Windows.Forms.Panel panel_pers_connecte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxMessages;
        private System.Windows.Forms.TextBox textBoxEnvoi;
        private System.Windows.Forms.Button btnEnvoi;
        private System.Windows.Forms.Timer timerUser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_Ami_connecte;
        private System.Windows.Forms.Button btnJoindre;
    }
}

