﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net; //pour utiliser les endpoint
using System.Net.Sockets; //pour utiliser les sockets
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP2_reseau
{
    public partial class Chat_TP2 : Form
    {
        //Création d'une classe "Usagers".
        //Permettra une association NOM-IP
        public class Usagers
        {
            public string Nom;
            public string IP;
        }

        //Création des sockets, écoute et envoi (udp)
        public Socket sckBroadcastEcoute;
        public Socket sckBroadcastEnvoi;
        public Socket sckTampon;

        //création des sockets.                                               (tcp)
        //sckEcoute : permet d'attendre une demande de connexion
        public Socket sckEcoute;
        //Serveur : socket utiliser par le client pour communiquer (sckServeur du client = sckEcoute du serveur)  tcp)
        public Socket sckServeur;
        //Client : socket recu sur sckEcoute pour pouvoir communiquer avec le client                               (tcp)
        public Socket sckClient;



        //Création des EndPoint écoute et envoi                        (udp)
        //Endpoint est "abstract", sert de base pour une autre classe  (udp)
        EndPoint epBroadcastEcoute, epBroadcastEnvoi, epTampon;

        //création des références pour les endPoint                      (tcp)
        EndPoint epLocal, epDistant;

        //servira a indiquer si c'est le serveur ou le client qui a démarré le formulaire "Chat"        (tcp)
        public bool serveur;

        //Servira à indiquer que la connection tcp est activée
        public bool reponse_tcp = false;

        //buffer de réception
        byte[] buffer;

        Int32 portBC = 55000;

        Usagers moi;

        //liste qui servira à savoir qui est en ligne                 (udp)
        public static List<Usagers> listeUsers = new List<Usagers>();

        private void btnConnexion_Click(object sender, EventArgs e)
        {
            moi = new Usagers();
            moi.Nom = textBoxNom.Text;
            textBoxNom.ReadOnly = true;
            btnConnexion.Enabled = false;

            //Définition des sockets. Sockets UDP commme pour le chat UDP     (udp)                   
            sckBroadcastEcoute = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sckBroadcastEnvoi = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sckTampon = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //Configuration de base des sockets    (udp)
            sckBroadcastEcoute.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            sckBroadcastEnvoi.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);

            sckTampon.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            //connexion du socket de broadcast en écoute   (udp)
            //IPEndPoint(IPAddress.Any, 55000) signifie que l'on écoute TOUTES les adresses IP  (udp)
            //qui parlent sur le port 55000    (udp)
            epBroadcastEcoute = new IPEndPoint(IPAddress.Any, portBC);

            //socket.Bind permet de donner une configuration à notre "bout de connexion"  (udp)
            sckBroadcastEcoute.Bind(epBroadcastEcoute);

            //Tableau qui permettra la réception des paquets  (udp)
            buffer = new byte[1024];

            //Début de l'écoute   (udp)
            try
            {
                sckBroadcastEcoute.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref epBroadcastEcoute, new AsyncCallback(callBackBroadcastEcoute), buffer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); //À ajouter pour le débuggage
            }

            //connexion du socket de broadcast en écriture          (udp)
            //IPEndPoint(IPAddress.Broadcast, 55000) signifie que l'on envoie le paquet   (udp)
            //à l'adresse de diffusion sur le port 55000                                  (udp)
            epBroadcastEnvoi = new IPEndPoint(IPAddress.Broadcast, portBC);

            //socket.Connect permet de donner la configuration du port "à l'autre bout"   (udp)
            sckBroadcastEnvoi.Connect(epBroadcastEnvoi);

            //affichage du début de connexion dans la console                    (udp)
            listBoxMessages.Items.Add("Système : Début de conversation");

            //Envoie d'une demande pour trouver les autres usagers               (udp)
            demandeInfo();

            //démarrage du compteur pour renouveller la liste d'utilisateurs     (udp)
            timerUser.Enabled = true;


            if (reponse_tcp){
                //Définition du socket
                //AdressFamily : protocole d'adressage utilisé (InterNetwork = IPv4)
                //SocketType : type de connection, data tranféré (Stream premet d'envoyer un flux d'information (utilisé pour TCP))
                //ProtocolType : protocole de transport utilisé
                sckServeur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //Création d'un Endpoint distant
                epDistant = new IPEndPoint(IPAddress.Parse(moi.IP), portBC);          //   (tcp)  revoir adresse

                //Requête de connexion. Un numéro de port aléatoire sera envoyé au serveur.
                //Si la requête est acceptée, la communication TCP pourra commencer.
                try
                {
                    sckServeur.Connect(epDistant);
                }
                catch //(Exception ex)
                {
                    //MessageBox.Show(ex.ToString()); //pour débogage
                   // listBoxSystem.Items.Add("Système : Erreur de connexion.");
                    listBoxMessages.Items.Add("Système : Erreur de connexion.");
                }

                //création d'une référence et d'un objet de type formulaire Chat
                chat_TCP chat = new chat_TCP(this);

                //Permettra de choisir le bon socket à utiliser pour communiquer
                serveur = false;

                //Ouvre le formulaire Chat et cache le formulaire actuel (login)
                chat.Show();
                this.Hide();

            }
        }


        //réception asynchrone du broadcast                                    (udp)
        private void callBackBroadcastEcoute(IAsyncResult ar)
        {
            //Vérifie si le socket est connecté.          (udp)

            //Invoke permet de créer un "delegate anonyme". Ceci permet de modifier quelque chose gérer par
            //un autre thread (listBox, textBox) asunchrone. Une "copie" est créer et le résultat est poussé    (udp)
            //au "vrai".
            Invoke((MethodInvoker)delegate
            {
                try
                {
                    //Création d'un tableau pour traiter le data
                    //le Data reçu est contenu dans l'argument "ar"             (udp)
                    byte[] receiveData = new byte[1204];
                    //AsyncState correspond au "State" de la fonction sckBoradcastEcoute.BeginReceiveFrom(...)   (udp)
                    //Finalement, on met "buffer" dans "receiveData"
                    receiveData = (byte[])ar.AsyncState;
                    //Convertion des codes ASCII en string                                                       (udp)
                    ASCIIEncoding aEncodig = new ASCIIEncoding();
                    string receivedMessage = aEncodig.GetString(receiveData);

                    //Vérifie si le message est une demande de nom, une réponse de nom ou un message
                    //nomReponse sera "true" si le message reçu commence par "Nom:"
                    //nomDemande sera "true" si le message reçu commence par "Qui:"
                    bool nomReponse = receivedMessage.StartsWith("Nom:"); //                                    (udp)
                    bool nomDemande = receivedMessage.StartsWith("Qui:"); // demande                            (udp)

                    //variable tampon qui sera utilisée pour faire une liste
                    //d'usagers temporaire
                    Usagers tampon = new Usagers();

                    //Si le message est une réponse (quelqu'un nous envoi ses info)                             (udp)
                    if (nomReponse)
                    {
                        //nomUtilisateur est un tableau de string
                        //la fonction "Split" permet de séparer les éléments d'une string
                        //en utilisant un caratère (ici, ":")
                        //le résultat sera string[0]= "Nom" et string[1] = le nom de l'utilisateur
                        string[] nomUtilisateur = receivedMessage.Split(':');
                        tampon.Nom = nomUtilisateur[1];
                        tampon.IP = nomUtilisateur[2];

                        //Ce teste évite d'avoir plusieurs fois le même nom dans la liste.
                        //Plusieur chat peuvent faire la demande "Qui". Ce qui dupliquera les réponses.      (udp)
                        //Nous ne voulons qu'une seule fois le nom de chaque usager.
                        //Donc, avant de l'ajouter, on regarde s'il y est.
                        if (!(listeUsers.Exists(x => x.Nom == tampon.Nom)))
                        {
                            listeUsers.Add(new Usagers() { Nom = tampon.Nom , IP = tampon.IP });
                        }
                    }
                    //Le message reçu est une demande d'info, on envoie notre nom                   (udp)
                    //avec la fonction "envoiInfo
                    else if (nomDemande)
                    {
                        envoiInfo();
                    }
                    //Sinon, c'est que le message est un message                                   (udp)
                    else
                    {
                        listBoxMessages.Items.Add(receivedMessage);
                    }

                    //redémarrage de l'écoute
                    buffer = new byte[1024];
                    sckBroadcastEcoute.BeginReceiveMessageFrom(buffer, 0, buffer.Length, SocketFlags.None, ref epBroadcastEcoute, new AsyncCallback(callBackBroadcastEcoute), buffer);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString()); //À ajouter pour débuggage
                }
            });
        }



        //Fonction pour demander qui est présent sur le chat                   (udp)
        private void demandeInfo()
        {
            //envoie d'une demande d'utilisateurs présents
            //création d'un ASCIIEncoding pour convertir string-ASCII        (udp)
            ASCIIEncoding aEnconding = new ASCIIEncoding();
            //taille de l'envoie
            byte[] sendingMessage = new byte[1024];
            //conversion des données pour l'envoi
            //Le "Qui:" indiquera que nous voulons connaître les autres usagers        (udp)
            sendingMessage = aEnconding.GetBytes("Qui:" + textBoxNom.Text);

            //envoie du message sur le socket
            try
            {
                sckBroadcastEnvoi.Send(sendingMessage);
            }
            catch
            {
                listBoxMessages.Items.Add("Système : Impossible de faire la demande d'info");
            }

            //À chaque demande, nous mettons à jour la liste                  (udp)
            //Vindange de la liste
            listBoxUsers.Items.Clear();
            //ajout des utilisateurs dans la liste (fenêtre de chat)            (udp)
            listBoxUsers.BeginUpdate();
            foreach (Usagers nom in listeUsers)
            {
                if (nom != null)
                {
                    //Vérifie si le nom reçu est le nôtre
                    //Trim() enlève les espace de début et fin
                    //Si ce n'est pas nous, le nom est ajouté à la liste d'usagers            (udp)
                    //l'argument "flase" indique que nous respectons la casse
                    if (!(String.Compare(moi.Nom, nom.Nom, false) == 0))
                    {
                        listBoxUsers.Items.Add(nom.Nom);
                    }
                }
            }
            listBoxUsers.EndUpdate();

            //On vide "listeUsagers" pour recevoir les prochaines info               (udp)
            listeUsers.Clear();
        }




        //Réponse envoyée à une demande d'info                   (udp)
        private void envoiInfo()
        {
            //envoie du nom d'utilisateur
            //création d'un ASCIIEncoding pour convertir string-ASCII               (udp)
            ASCIIEncoding aEnconding = new ASCIIEncoding();
            //taille de l'envoie
            byte[] sendingMessage = new byte[1024];

            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            //Pour chaque adresses trouvées dans le tableau (normalement sur un PC il n'y en a qu'une),
            //nous vérifions s'il s'agit d'une adresse IPv4 (InterNetwork)                                  (udp)
            //L'adresse IPv4 trouvée sera entrée dans le champs de IP de "Moi"
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    moi.IP= address.ToString();
                    
                }
            }

            //conversion des données pour l'envoi
            //Le "Nom:" indiquera que nous envoyons notre nom aux autres usagers        (udp)
            sendingMessage = aEnconding.GetBytes("Nom:" + moi.Nom + ":" + moi.IP);  

            //envoie du message sur le socket
            try
            {
                sckBroadcastEnvoi.Send(sendingMessage);
            }
            catch
            {
                listBoxMessages.Items.Add("Système : Impossible d'envoyer les info");
            }
        }

        private void btnEnvoi_Click(object sender, EventArgs e)
        {
            //La classe "ASCIIEncoding" permet des fonctions de conversion ASCII             (udp)
            ASCIIEncoding aEnconding = new ASCIIEncoding();
            //taille de l'envoie
            byte[] sendingMessage = new byte[1024];
            //Conversion
            sendingMessage = aEnconding.GetBytes(textBoxNom.Text.Trim() + ":" + textBoxEnvoi.Text);

            //envoie du message sur le socket                    (udp)
            try
            {

                foreach (Usagers nom in listeUsers)
                {
                    if (nom != null)
                    {
                        epTampon = new IPEndPoint(IPAddress.Parse(nom.IP), portBC);
                        sckTampon.Connect(epTampon);
                        sckTampon.Send(sendingMessage);
                    }
                }

                //ajout dans la fenêtre de chat                            (udp)
                listBoxMessages.Items.Add("Moi : " + textBoxEnvoi.Text);
                textBoxEnvoi.Text = "";
            }
            catch
            {
                listBoxMessages.Items.Add("Système : Impossible d'envoyer le message");
            }
        }

        private void textBoxEnvoi_KeyDown(object sender, KeyEventArgs e)
        {
            //On vérifie si la touche appuyée est "Enter"               (udp)
            if (e.KeyCode == Keys.Enter)
            {
                //La classe "ASCIIEncoding" permet des fonctions de conversion ASCII             (udp)
                ASCIIEncoding aEnconding = new ASCIIEncoding();
                //taille de l'envoie
                byte[] sendingMessage = new byte[1024];
                //Conversion
                sendingMessage = aEnconding.GetBytes(textBoxNom.Text.Trim() + ":" + textBoxEnvoi.Text);

                //envoie du message sur le socket                     (udp)
                try
                {
                    sckBroadcastEnvoi.Send(sendingMessage);
                    //ajout dans la fenêtre de chat
                    listBoxMessages.Items.Add("Moi : " + textBoxEnvoi.Text);
                    textBoxEnvoi.Text = "";
                }
                catch
                {
                    listBoxMessages.Items.Add("Système : Impossible d'envoyer le message");
                }
            }

        }


        //Exécuté à la fermeture du programme                     (udp)
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //fermeture "propre" des sockets                   (udp)
                //Arrêt d'envoi/réception
                sckBroadcastEcoute.Shutdown(SocketShutdown.Both);
                sckBroadcastEnvoi.Shutdown(SocketShutdown.Both);
                //Libération des ressources                         (udp)
                sckBroadcastEcoute.Close(); 
                sckBroadcastEnvoi.Close();

            }
            catch
            {

            }
        }

        private void btnDeconnexion_Click(object sender, EventArgs e)
        {
            try
            {
                //fermeture "propre" des sockets                  (udp)
                //Arrêt d'envoi/réception
                sckBroadcastEcoute.Shutdown(SocketShutdown.Both);
                sckBroadcastEnvoi.Shutdown(SocketShutdown.Both);
                //Libération des ressources                       (udp)
                sckBroadcastEcoute.Close();
                sckBroadcastEnvoi.Close();
                //désactive le timer
                timerUser.Enabled = false;

                textBoxNom.ReadOnly = false;
                btnConnexion.Enabled = true;

                //indiquer la déconnection dans la fenêtre de messages         (udp)
                listBoxMessages.Items.Add("Système : Déconnecté");
            }
            catch
            {
                listBoxMessages.Items.Add("Système : Erreur");
            }
        }

        //L'ajout d'un "async" permettra de mettre un "await" qui permettra d'effectuer une opération
        //de façon asynchrone. La méthode s'exécutera de façon synchronne jusqu'à son premier "await".  (tcp)
        //Sans ce "async" et ce "await", l'ordinateur gèlerait pendant l'écoute sur le socket.
        private async void btnJoindre_Click(object sender, EventArgs e)
        {
            reponse_tcp = true;
        
            //Définition des sockets                (tcp)
            //AdressFamily : protocole d'adressage utilisé (InterNetwork = IPv4)
            //SocketType : type de connection, data tranféré (Stream premet d'envoyer un flux d'information (utilisé pour TCP))
            //ProtocolType : protocole de transport utilisé
            sckEcoute = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sckClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Définition du EndPoint local pour l'écoute
            //champs : adresse IP, port
            //IPAdresse.Parse(permet de passer d'une string 255.255.255.255 vers le format IPAdress)
            //Convert.ToInt32 permet de convertir une string en int (pour le numéro de port)
            epLocal = new IPEndPoint(IPAddress.Parse(moi.IP),portBC);   //  (tcp)
            //association du point de terminaison local du socket
            //socket.Bind permet de donner une configuration à notre "bout de connexion"
            sckEcoute.Bind(epLocal);
            //mettre le socket en écoute, 3 requêtes possibles dans la file, les autres seront rejetés
            //et receveront un message "serveur busy"
            sckEcoute.Listen(3);

            //Informe l'utilisateur du début de l'écoute
           // listBoxSystem.Items.Add("Système : Écoute démarrée.");
            listBoxMessages.Items.Add("Système : Écoute démarrée.");

            //démarre un Task (Task.Factory.StartNew) asynchrone (await) dans lequel on écoute le socket.
            //Lorsque "Accept()" retournera une réponse (un socket),
            //la réponse sera mise dans sckClient (socket du Client(celui qui fait une requête de connexion)).
            //"() => sckEcoute.Accept())" : méthode exécutée par le task.
            //La valeur retournée, au lieu d'être un task, sera convertie au type de sckClient (Socket)
            sckClient = await Task.Factory.StartNew(() => sckEcoute.Accept());

            //création d'une référence et d'un objet de type formulaire Chat
            chat_TCP chat = new chat_TCP(this);

            //Permettra de choisir le bon socket à utiliser pour communiquer
            serveur = true;

            //Ouvre le formulaire Chat et cache le formulaire actuel (login)
            chat.Show();
            this.Hide();
        }

        private void listBoxUsers_DoubleClick(object sender, EventArgs e)
        {

        }

        private void timerUser_Tick(object sender, EventArgs e)
        {
            demandeInfo();
        }

        public Chat_TP2()
        {
            InitializeComponent();
        }
    }
}
