﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net; //pour utiliser les endpoint
using System.Net.Sockets; //pour utiliser les sockets
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP2_reseau
{
    public partial class chat_TCP : Form
    {

        //création du socket de communication
        //le socket client ou serveur sera "copié" dans ce socket
        public Socket sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        //création d'une référence de type Login (formulaire)
        public Chat_TP2 chat_tp2;

        //Création d'un buffer pour la réception et l'envoi
        byte[] buffer;

        //"Chat_TP2 chat_tp2" permet de passer le formulaire "chat_tp2" au formulaire "chat"
        public chat_TCP(Chat_TP2 chat_tp2)
        {
            //prend les valeurs de chat_tp2 et les met dans l'objet chat_tp2 de CE formulaire
            this.chat_tp2 = chat_tp2;
            InitializeComponent();
        }

        //Fait à l'ouverture du formulaire
        private void Chat_Load(object sender, EventArgs e)
        {
            //Vérifie si c'est le serveur ou le client qui a démarré le chat
            //Met le bon sck en conséquence.
            if (chat_tp2.serveur)
            {
                sck = chat_tp2.sckClient;
            }
            else
            {
                sck = chat_tp2.sckServeur;
            }

            //Création d'un buffer de 1024 bytes pour la réception
            buffer = new byte[1024];
            try
            {
                //Nous devons démarrer la réception asynchrone sur le socket. Si la réception n'est pas asynchrone,
                //le CPU ne pourra pas faire autre chose. L'ordi va geler.
                //options : endroit de réception (buffer), à quel endroit dans "buffer" commençons-nous la réception (0 = début),
                //la longueur du data à recevoir (buffer.Length), les "flags" utilisés (ici, aucun),
                //la fonction appelée une fois la réception effectuée,
                //"state" : contient l'information passée en argument à la fonction AsyncCallBack (ici : buffer)
                sck.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
            }
            catch
            {
                listBoxMessages_TCP.Items.Add("Système : Erreur de réception.");
            }
        }

        //Fonction CallBack
        private void ReceiveCallBack(IAsyncResult ar)
        {
            //Nouveau tableau de bytes
            byte[] receiveData = new byte[1024];
            //On met les bytes reçus dans notre tableau
            receiveData = (byte[])ar.AsyncState;

            //La classe ASCIIEncoding permet les conversions
            ASCIIEncoding aEncoding = new ASCIIEncoding();
            //conversion bytes -> string
            string receivedMessage = aEncoding.GetString(receiveData);

            //affiche le message
            listBoxMessages_TCP.Items.Add("Ami(e) : " + receivedMessage);

            //Nouvel objet
            buffer = new byte[1204];
            //redémarrage de l'écoute
            try
            {
                sck.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
            }
            catch
            {
                listBoxMessages_TCP.Items.Add("Système : Erreur de réception.");
            }
        }

        //Fonction d'envoi exécutée lors du click tu bouton envoi
        private void buttonEnvoi_Click(object sender, EventArgs e)
        {
            //Pour la conversion
            ASCIIEncoding aEncoding = new ASCIIEncoding();
            //Buffer d'envoi
            byte[] envoiData = new byte[1024];

            //transforme la string de la textBox Envoi en tableau d'octets
            envoiData = aEncoding.GetBytes(textBoxEnvoi_TCP.Text);

            try
            {
                //envoie du message sur le socket
                //En TCP, il faut indiquer la longueur pour la confirmation de réception et le contrôle de flux
                //Il faut également indiquer les "flags", ils ne sont pas utilisés ici
                sck.Send(envoiData, envoiData.Length, SocketFlags.None);
                //Affiche le message envoyé
                listBoxMessages_TCP.Items.Add("Moi : " + textBoxEnvoi_TCP.Text);
                //Efface le contenu de la textbox Envoi
                textBoxEnvoi_TCP.Text = "";
            }
            catch
            {
                listBoxMessages_TCP.Items.Add("Sytème : Impossible d'envoyer le message.");
            }
        }

        //À la fermeture du formulaire
        private void Chat_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Arrêt de la réception et de l'envoie sur ce socket
            try
            {
                sck.Shutdown(SocketShutdown.Both);
            }
            catch { }
            //Fermeture de ce socket
            try
            {
                sck.Close();
            }
            catch { }
            //Fermeture du formulaire principal (Login)
            try
            {
                chat_tp2.Close();
            }
            catch { }
        }

        //Fonction d'envoi exécutée lorsque "Enter" est appuyé
        private void textBoxEnvoi_KeyDown(object sender, KeyEventArgs e)
        {
            //On vérifie si c'est "Enter" qui a été appuyé
            if (e.KeyCode == Keys.Enter)
            {
                //Pour la conversion
                ASCIIEncoding aEncoding = new ASCIIEncoding();
                //Buffer d'envoi
                byte[] envoiData = new byte[1024];

                //transforme la string de la textBox Envoi en tableau d'octets
                envoiData = aEncoding.GetBytes(textBoxEnvoi_TCP.Text);

                try
                {
                    //envoie du message sur le socket
                    //En TCP, il faut indiquer la longueur pour la confirmation de réception et le contrôle de flux
                    //Il faut également indiquer les "flags" qui ne sont pas utilisés ici
                    sck.Send(envoiData, envoiData.Length, SocketFlags.None);
                    //Affiche le message envoyé
                    listBoxMessages_TCP.Items.Add("Moi : " + textBoxEnvoi_TCP.Text);
                    //Efface le contenu de la textbox Envoi
                    textBoxEnvoi_TCP.Text = "";
                }
                catch
                {
                    listBoxMessages_TCP.Items.Add("Sytème : Impossible d'envoyer le message.");
                }
            }
        }


    }
}
