﻿namespace TP2_reseau
{
    partial class chat_TCP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxMessages_TCP = new System.Windows.Forms.ListBox();
            this.textBoxEnvoi_TCP = new System.Windows.Forms.TextBox();
            this.btnEnvoi_TCP = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxMessages_TCP
            // 
            this.listBoxMessages_TCP.FormattingEnabled = true;
            this.listBoxMessages_TCP.Location = new System.Drawing.Point(12, 12);
            this.listBoxMessages_TCP.Name = "listBoxMessages_TCP";
            this.listBoxMessages_TCP.Size = new System.Drawing.Size(446, 212);
            this.listBoxMessages_TCP.TabIndex = 7;
            // 
            // textBoxEnvoi_TCP
            // 
            this.textBoxEnvoi_TCP.Location = new System.Drawing.Point(12, 230);
            this.textBoxEnvoi_TCP.Name = "textBoxEnvoi_TCP";
            this.textBoxEnvoi_TCP.Size = new System.Drawing.Size(359, 20);
            this.textBoxEnvoi_TCP.TabIndex = 9;
            // 
            // btnEnvoi_TCP
            // 
            this.btnEnvoi_TCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnvoi_TCP.Location = new System.Drawing.Point(377, 230);
            this.btnEnvoi_TCP.Name = "btnEnvoi_TCP";
            this.btnEnvoi_TCP.Size = new System.Drawing.Size(81, 20);
            this.btnEnvoi_TCP.TabIndex = 10;
            this.btnEnvoi_TCP.Text = "Envoi_TCP";
            this.btnEnvoi_TCP.UseVisualStyleBackColor = true;
            // 
            // chat_TCP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 256);
            this.Controls.Add(this.btnEnvoi_TCP);
            this.Controls.Add(this.textBoxEnvoi_TCP);
            this.Controls.Add(this.listBoxMessages_TCP);
            this.Name = "chat_TCP";
            this.Text = "chat_TCP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxMessages_TCP;
        private System.Windows.Forms.TextBox textBoxEnvoi_TCP;
        private System.Windows.Forms.Button btnEnvoi_TCP;
    }
}